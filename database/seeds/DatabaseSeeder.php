<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function () {

            # Make 3 Organizations
            $organizations = factory(App\Models\Organization::class, 3)->create();

            foreach ($organizations as $organization) {

                # Make one Manager and three Persons for each Organization

                $manager = factory(App\Models\User::class)->create([
                    'organization_id' => $organization->id, # Laravel will take the ID
                    'is_manager' => true,
                ]);

                $organization->manager_id = $manager->id;
                $organization->save();

                $persons = factory(App\Models\User::class, 3)->create([
                    'organization_id' => $organization, # Laravel will take the ID
                    'is_person' => true,
                ]);

            }

        });

    }
}
