<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Organization::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->company,
        'phone' => $faker->unique()->phoneNumber,
        'email' => $faker->unique()->safeEmail,
        'website' => $faker->unique()->domainName,
        'logo' => $faker->image('public/storage/images', 500, 500, null, false),
    ];
});
