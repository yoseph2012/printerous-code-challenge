<?php

namespace App\Models;

use App\Scopes\ManagerScope;

class Manager extends User
{
    /**
     * Explicitly defines table name.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new ManagerScope());
    }
}
