const mix = require('laravel-mix');
const CleanWebpackPlugin = require('clean-webpack-plugin');

// Cleaning
mix.webpackConfig({
    plugins: [
        new CleanWebpackPlugin(['public/assets/js', 'public/assets/css',], {})
    ]
});

// Vendor CSSes
mix.styles([
    'node_modules/bootstrap/dist/css/bootstrap.min.css',
], 'public/assets/css/vendor.css').version();

// Vendor JSes
mix.scripts([
    'node_modules/jquery/dist/jquery.min.js',
    'node_modules/bootstrap/dist/js/bootstrap.min.js',
], 'public/assets/js/vendor.js').version();

