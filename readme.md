# Printerous Code Challenge

## Running The App
This app using PHP 7.2 and Percona 5.7. To run the app using Docker:
- Make sure you have [Docker Sync](http://docker-sync.io/). If you don't want to use Docker Sync or you're not a Mac user, modify `docker-compose.yml` file.
- Clone the repo (example: /Users/username/Printerous)
- Run installs:
  - `$ composer install`
  - `$ npm install`
  - `$ npm run dev`
- Run Docker:
  - `$ docker-sync start`
  - `$ docker-compose up -d --build`
- Check main application container ID with `$ docker ps` and go inside the container, for example: `$ docker exec -it xxx /bin/bash` where `xxx` is the ID of the container.
- Inside the container, travel to `/srv/app`, copy the .env.example file, generate app key, and run migration and seeder:
  - `$ cd /srv/app`
  - `$ cp .env.example .env`
  - `$ php artisan key:generate`
  - `$ php artisan migrate --seed`.
- Go to `localhost:8098` on your browser.


## Specification

- User harus login menggunakan email dan password
- User dapat melakukan pencarian dengan satu keyword untuk mencari berdasarkan nama Organization atau nama Person.
- Organization properties: name, phone, email, website, logo (upload image)
- Person properties: name, email, phone, avatar (upload image)
- Manager properties (assumed):  name, email, phone, avatar (upload image)
- Organization has many Person
- Organization has one Manager (assumed)
- Person ditampilkan pada halaman detil Organization
- Manager hanya bisa me-manage (create, update, delete) sebuah Organization jika dia adalah Manager dari Organization tersebut.
- Manager juga dapat menambahkan, memperbarui dan menghapus list Person dari sebuah Organization.
- Jika logged in User yang bukan merupakan Manager dari Organization, maka dia hanya dapat melihat data Organization tersebut (assuming that Person list will be hidden).
- Silakan gunakan Framework CSS seperti bootstrap untuk membuat tampilan lebih baik.

## Submission
- To: wawan@printerous.com
- Subject: Printerous Code Challenge - Yoseph Daryana
- Body: Send repository URL

